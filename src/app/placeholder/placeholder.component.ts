import { Component, OnInit } from '@angular/core';
import {Http, Headers} from '@angular/http';

@Component({
  selector: 'app-placeholder',
  templateUrl: './placeholder.component.html',
  styleUrls: ['./placeholder.component.css']
})
export class PlaceholderComponent implements OnInit {
  results: Response;

  constructor(private http: Http) { }

  ngOnInit() {
    this.getPosts();
  
  }

  getPosts(){
    this.http.get('https://jsonplaceholder.typicode.com/posts/').subscribe(data => {
      this.results = data.json();
      });
  }
  

}
