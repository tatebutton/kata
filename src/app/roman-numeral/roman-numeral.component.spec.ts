import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RomanNumeralComponent } from './roman-numeral.component';

describe('RomanNumeralComponent', () => {
  let component: RomanNumeralComponent;
  let fixture: ComponentFixture<RomanNumeralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RomanNumeralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RomanNumeralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
