import { Component, OnInit } from '@angular/core';
import {Http, Headers} from '@angular/http';

@Component({
  selector: 'app-roman-numeral',
  templateUrl: './roman-numeral.component.html',
  styleUrls: ['./roman-numeral.component.css']
})
export class RomanNumeralComponent implements OnInit {

  inputNumber: number;
  result: string;

  constructor(private http: Http) { }

  ngOnInit() {
  }

  getRomanNumeral(){
    this.http.get('http://localhost:51028/api/RomanNumeral/ArabicToRoman/' + this.inputNumber).subscribe(data => {
      this.result = data.json();
      });
  }

}
