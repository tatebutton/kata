import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {
  
  inputNumber: number;
  result: number;

  constructor() {
   }

   multiply(){
     this.result = this.inputNumber * 2;
   }

  ngOnInit() {
  }

}
