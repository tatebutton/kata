import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ValuesComponent } from './values/values.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { PlaceholderComponent } from './placeholder/placeholder.component';
import { RomanNumeralComponent } from './roman-numeral/roman-numeral.component';


@NgModule({
  declarations: [
    AppComponent,
    ValuesComponent,
    CalculatorComponent,
    PlaceholderComponent,
    RomanNumeralComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
